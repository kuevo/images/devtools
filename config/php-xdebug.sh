#!/bin/sh

set -e

php -d zend_extension=xdebug.so -c /etc/php/PHPVERSION/cli/php-xdebug.ini "$@"
