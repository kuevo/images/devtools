# syntax=docker/dockerfile:1.4
ARG COMPOSER_VERSION="2.2"
FROM composer:${COMPOSER_VERSION} AS composer
FROM bitnami/minideb:latest

# Run as root
USER root

# Non interactive
ENV DEBIAN_FRONTEND=noninteractive

# Copy in the entrypoint
COPY --link entrypoint.sh /entrypoint.sh

# Copy in the commands in commands to /usr/local/bin
COPY --link commands/* /usr/local/bin/

# Copy in the composer package
COPY --link --from=composer /usr/bin/composer /usr/bin/composer

# Copy in the php-xdebug.sh file to /usr/local/bin/php-xdebug
COPY --link config/php-xdebug.sh /usr/local/bin/php-xdebug

# Install the various packages
ARG PHP_VERSION="8.2"
ARG NODE_VERSION="16"
ENV COMPOSER_ALLOW_SUPERUSER="1"
ENV COMPOSER_MEMORY_LIMIT="-1"
ENV COMPOSER_CACHE_DIR="/tmp/.composer"
ENV COMPOSER_PROCESS_TIMEOUT="9001"
RUN \
    apt update && \
    apt upgrade -y && \
    install_packages \
        software-properties-common \
        gettext-base \
        patch \
        wget \
        curl \
        git \
        default-mysql-client \
        p7zip-full \
        net-tools \
        htop \
        unzip \
        zsh \
        nano \
        procps \
        supervisor \
        build-essential \
        gpg \
        coreutils \
        iputils-ping \
    && \
    curl -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg && \
    curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | tee /usr/share/keyrings/yarnkey.gpg >/dev/null && \
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg && \
    echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list && \
    echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_VERSION.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list && \
    apt update && \
    install_packages \
        php${PHP_VERSION} \
        php${PHP_VERSION}-cli \
        php${PHP_VERSION}-readline \
        php${PHP_VERSION}-common \
        php${PHP_VERSION}-mbstring \
        php${PHP_VERSION}-igbinary \
        php${PHP_VERSION}-apcu \
        php${PHP_VERSION}-imagick \
        php${PHP_VERSION}-yaml \
        php${PHP_VERSION}-bcmath \
        php${PHP_VERSION}-mysql \
        php${PHP_VERSION}-mysqlnd \
        php${PHP_VERSION}-mysqli \
        php${PHP_VERSION}-zip \
        php${PHP_VERSION}-bz2 \
        php${PHP_VERSION}-gd \
        php${PHP_VERSION}-msgpack \
        php${PHP_VERSION}-intl \
        php${PHP_VERSION}-zstd \
        php${PHP_VERSION}-redis \
        php${PHP_VERSION}-curl \
        php${PHP_VERSION}-opcache \
        php${PHP_VERSION}-xml \
        php${PHP_VERSION}-soap \
        php${PHP_VERSION}-exif \
        php${PHP_VERSION}-xsl \
        php${PHP_VERSION}-gettext \
        php${PHP_VERSION}-cgi \
        php${PHP_VERSION}-dom \
        php${PHP_VERSION}-ftp \
        php${PHP_VERSION}-iconv \
        php${PHP_VERSION}-pdo \
        php${PHP_VERSION}-simplexml \
        php${PHP_VERSION}-tokenizer \
        php${PHP_VERSION}-xml \
        php${PHP_VERSION}-xmlwriter \
        php${PHP_VERSION}-xmlreader \
        php${PHP_VERSION}-fileinfo \
        php${PHP_VERSION}-xdebug \
        nodejs \
    && \
    # Remove the xdebug.ini to prevent xdebug from working unless we explicitly want it to
    rm /etc/php/${PHP_VERSION}/mods-available/xdebug.ini && \
    # Install magerun2
    wget https://files.magerun.net/n98-magerun2.phar && \
    mv n98-magerun2.phar /usr/local/bin/magerun2 && \
    chmod +x /usr/local/bin/magerun2 && \
    ln -s /usr/local/bin/magerun2 /usr/local/bin/magerun && \
    # Install extra packages
    sh -c "$(wget -O- https://github.com/deluan/zsh-in-docker/releases/download/v1.1.2/zsh-in-docker.sh)" -- -p git -p dircycle -p node \
        -p https://github.com/zsh-users/zsh-autosuggestions \
        -p https://github.com/zsh-users/zsh-completions \
        -p https://github.com/zsh-users/zsh-history-substring-search && \
    # Add /app/vendor/bin to path
    echo 'export PATH="$PATH:/app/vendor/bin"' >> /root/.zshrc && \
    # Shrink binaries
    (find /usr/local/bin -type f -print0 | xargs -n1 -0 strip --strip-all -p 2>/dev/null || true) && \
    (find /usr/local/lib -type f -print0 | xargs -n1 -0 strip --strip-all -p 2>/dev/null || true) && \
    # Make files executable
    chmod +x /entrypoint.sh /usr/local/bin/* && \
    # Replace PHPVERSION with the variable ${PHP_VERSION}
    sed -i "s/PHPVERSION/${PHP_VERSION}/g" /usr/local/bin/php-xdebug

# Copy in the module configurations
ARG PHP_VERSION="8.2"
COPY --link config/modules/* /etc/php/${PHP_VERSION}/mods-available/

# Copy in the production configs
ARG PHP_VERSION="8.2"
COPY --link config/php-xdebug.ini /etc/php/${PHP_VERSION}/cli/php-xdebug.ini
COPY --link config/php.ini /etc/php/${PHP_VERSION}/cli/php.ini

# Set workdir
WORKDIR /app

# Just sleep, so we can attach to the container
CMD [ "/entrypoint.sh" ]
