#!/bin/sh

set -e

echo "This is a development container, it does nothing, it does not emit any log output, it does not run any services, it does not start any daemons, it does not do anything at all."
echo "It is just a container that you can use to run commands in."
echo "\n"
echo "You enter the container using devspace, or directly using kubectl like so:"
echo "kubectl exec -it web -c devtools -- zsh"

# Wait till the /app/.kuevo/commands/background-hooks directory is done synchronizing from the host (also wait till there is at least a single file in it)
while [ ! -d /app/.kuevo/commands/background-hooks ] || [ ! -d /app/vendor ] || [ ! "$(ls -A /app/.kuevo/commands/background-hooks)" ]; do
    sleep 30
done

# If there is files in the /app/.kuevo/commands/background-hooks directory, then we need to run them in the background otherwise, don't run anything in the background
if [ "$(ls -A /app/.kuevo/commands/background-hooks)" ]; then
    # Generate a supervisor config for each background hook in .kuevo/commands/background-hooks
    for hook in /app/.kuevo/commands/background-hooks/*.sh; do
        echo "[program:$(basename $hook)]" > /etc/supervisor/conf.d/$(basename $hook).conf
        echo "command = $hook" >> /etc/supervisor/conf.d/$(basename $hook).conf
        echo "autostart = true" >> /etc/supervisor/conf.d/$(basename $hook).conf
        echo "autorestart = true" >> /etc/supervisor/conf.d/$(basename $hook).conf
        echo "stderr_logfile = /dev/fd/1" >> /etc/supervisor/conf.d/$(basename $hook).conf
        echo "stdout_logfile = /dev/fd/1" >> /etc/supervisor/conf.d/$(basename $hook).conf
        # Try 9001 times to start
        echo "startretries = 9001" >> /etc/supervisor/conf.d/$(basename $hook).conf
        # Wait 10s between each attempt to start
        echo "startsecs = 10" >> /etc/supervisor/conf.d/$(basename $hook).conf

        # chmod the hook so it can be executed
        chmod +x $hook
    done

    # Run supervisor in foreground
    supervisord -n -c /etc/supervisor/supervisord.conf
fi
